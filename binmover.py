import glob
import time
import shutil
import os
import tkinter as tk
from tkinter import filedialog
from datetime import datetime


def print_time():
    return str(datetime.now().time().hour).zfill(2) + ':' + str(datetime.now().time().minute).zfill(2) + ':' + str(datetime.now().time().second).zfill(2) + ' ---'


# Hide TK window
root = tk.Tk()
root.withdraw()

# Get directories

home_dir = os.path.expanduser('~')
watch_dir = filedialog.askdirectory(initialdir=home_dir + "/Downloads", mustexist=True, title="Select downloads folder")

target_dir = filedialog.askdirectory(initialdir="E:/", mustexist=True, title="Select K64F device directory")

if not watch_dir or not target_dir:
    print("No directory selected")
    exit(1)

print(print_time(), 'Running')

connected = True

while True:

    file = glob.glob(watch_dir + "/*K64F.bin")

    if file:
        fileName = os.path.basename(file[0])

        if connected:
            print(print_time(), fileName, 'found')

        try:
            shutil.move(file[0], target_dir + "/" + fileName)
            connected = True
            print(print_time(), fileName, 'has been moved')

        except FileNotFoundError:
            if connected:
                connected = False
                print(print_time(), 'Device is not connected!')

    time.sleep(1)
