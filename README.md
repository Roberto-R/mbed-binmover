# MBED BinMover

Python program to automatically move downloaded .bin files for the K64F to the MBED development board. Leave the program running in the background and it will keep moving your bin files.

## Requirements

 - Windows only.
     - When using the executable, no Python installation is required.
     - It should be easy enough to convert this to work with a Linux system.

## Usage

 - Either: 
     - [Download](https://bitbucket.org/Roberto-R/mbed-binmover/raw/master/binmover.zip) `binmover.zip`, extract it or open it and run `binmover.exe`.
     - OR clone the directory and run `python binmover.py`. Use `python -m pip install ...` to install missing packages if any missing package errors occur.
 - Select a watch directory, typically your browsers download directory.
 - Select a destination. Have your K64F connected and select the drive as which it shows up (typically `E:/` or whatever USB drives are named on your PC).
 - Keep the window open. Every second the downloads directory is scanned for .bin files and they will be moved to the MBED board. Remember to keep an eye on the K64F programming LED to prevent disconnected the board too soon.

## Info
 
  - `cx_Freeze` is used to compile the executable. Run `python .\setup.py build` to build it.