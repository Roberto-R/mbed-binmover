from cx_Freeze import setup, Executable

base = None

executables = [Executable("binmover.py", base=base)]

packages = ["glob", "time", "shutil", "os", "tkinter", "datetime"]
options = {
    'build_exe': {
        'packages': packages,
    },
}

setup(
    name="MBED BinMover",
    options=options,
    version="1.0.0",
    description='Automatically move bin files to your development board',
    executables=executables
)